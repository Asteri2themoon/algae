import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import torch
import torch.nn as nn

from custom_adam import LREQAdam
from nn.reshape import Reshape, DebugShape
from nn.noise import Noise


def init_weights(m):
    if type(m) == nn.Linear:
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)
    elif type(m) == nn.Conv2d:
        torch.nn.init.kaiming_uniform_(m.weight)
        m.bias.data.fill_(0.01)


class G(nn.Module):
    def __init__(
        self,
        latent_size=64,
        kernels=[256, 256, 256],  # , 256
        image_size=(16, 16),
    ):
        super(G, self).__init__()

        first_w = image_size[0] >> (len(kernels) - 1)
        first_h = image_size[1] >> (len(kernels) - 1)

        layers = []

        layers.append(nn.Linear(latent_size, first_w * first_h * kernels[0]))
        layers.append(nn.LeakyReLU())
        layers.append(Reshape((kernels[0], first_h, first_w)))

        def conv_layer(layers, h_in, h_out, activation=nn.LeakyReLU(), stride=2):
            if stride != 1:
                # layers.append(Noise(noise_std))
                layers.append(nn.Conv2d(h_in, h_out * stride ** 2, 3, padding=1))
                layers.append(nn.PixelShuffle(stride))
                layers.append(nn.BatchNorm2d(h_out))
                layers.append(nn.LeakyReLU())
                h_in = h_out
            if h_out != 1:
                # layers.append(Noise(noise_std))
                layers.append(nn.Conv2d(h_in, h_out, 3, padding=1))
                layers.append(nn.BatchNorm2d(h_out))
                layers.append(nn.LeakyReLU())
                h_in = h_out

            # layers.append(Noise(noise_std))
            layers.append(nn.Conv2d(h_in, h_out, 3, padding=1))
            layers.append(nn.BatchNorm2d(h_out))
            layers.append(activation)

        conv_layer(layers, kernels[0], kernels[0], stride=1)

        for h_in, h_out in zip(kernels[:-1], kernels[1:]):
            conv_layer(layers, h_in, h_out)

        conv_layer(layers, kernels[-1], 1, activation=nn.Tanh(), stride=1)

        self.net = nn.Sequential(*layers)

        self.apply(init_weights)

    def forward(self, x):
        return self.net(x)


class E(nn.Module):
    def __init__(
        self, image_size=(16, 16), kernels=[256, 256, 256], latent_size=64
    ):  # , 256
        super(E, self).__init__()

        self.z_size = latent_size

        first_w = image_size[0] >> (len(kernels) - 1)
        first_h = image_size[1] >> (len(kernels) - 1)

        layers = []

        layers.append(nn.Conv2d(1, kernels[0], 3, padding=1))
        layers.append(nn.LeakyReLU())

        for h_in, h_out in zip(kernels[:-1], kernels[1:]):
            layers.append(nn.Conv2d(h_in, h_out, 3, padding=1, stride=2))
            layers.append(nn.LeakyReLU())

        layers.append(nn.Flatten())
        layers.append(
            nn.Linear(first_w * first_h * kernels[-1], latent_size, bias=True)
        )
        layers.append(nn.LeakyReLU())

        self.mean = nn.Linear(latent_size, latent_size)
        self.std = nn.Linear(latent_size, latent_size)

        self.net = nn.Sequential(*layers)

        self.apply(init_weights)

    def forward(self, x, sample=True):
        h = self.net(x)

        mean = self.mean(h)

        if sample:
            std = self.std(h)
            return mean + std * torch.randn_like(std), mean, std
        else:
            return mean


class D(nn.Module):
    def __init__(
        self, image_size=(16, 16), kernels=[256, 256, 256], dense=[64]
    ):  # , 256
        super(D, self).__init__()

        self.activation = []

        first_w = image_size[0] >> (len(kernels) - 1)
        first_h = image_size[1] >> (len(kernels) - 1)

        layers = []

        def conv_layer(layers, h_in, h_out, activation=nn.LeakyReLU(), stride=2):
            if stride != 1:
                layers.append(nn.Conv2d(h_in, h_out, 3, padding=1, stride=stride))
                layers.append(nn.LeakyReLU())
                h_in = h_out
            if h_out != 1:
                layers.append(nn.Conv2d(h_in, h_out, 3, padding=1))
                layers.append(nn.LeakyReLU())
                h_in = h_out

            layers.append(nn.Conv2d(h_in, h_out, 3, padding=1))
            layers.append(activation)

        conv_layer(layers, 1, kernels[0], stride=1)
        # layers.append(nn.Conv2d(1, kernels[0], 3, padding=1))
        # layers.append(nn.LeakyReLU())

        for h_in, h_out in zip(kernels[:-1], kernels[1:]):
            conv_layer(layers, h_in, h_out, stride=2)
            # layers.append(nn.Conv2d(h_in, h_out, 3, padding=1, stride=2))
            # layers.append(nn.LeakyReLU())

        layers.append(nn.Flatten())
        layers.append(nn.Linear(first_w * first_h * kernels[-1], dense[0], bias=True))
        layers.append(nn.LeakyReLU())

        for h_in, h_out in zip(dense[:-1], dense[1:]):
            layers.append(nn.Linear(h_in, h_out, bias=True))
            layers.append(nn.LeakyReLU())

        layers.append(nn.Linear(dense[-1], 1))

        self.net = nn.ModuleList(layers)

        self.apply(init_weights)

    def forward(self, x, save_activation=False):
        if save_activation:
            self.activation = []

        h = x
        for layer in self.net:
            h = layer(h)
            if type(layer) == nn.Conv2d and save_activation:
                self.activation.append(
                    torch.cat([h.mean(dim=(2, 3)), h.std(dim=(2, 3))])
                )

        return h


def cirtics_activation(x):
    # return x
    return nn.functional.softplus(x)


class VAEGAN(nn.Module):
    def __init__(self, g=G(), e=E(), d=D()):
        super(VAEGAN, self).__init__()
        self.g = g
        self.e = e
        self.d = d

    def latent_loss(self, z_mean, z_stddev):
        mean_sq = z_mean * z_mean
        stddev_sq = z_stddev * z_stddev
        return 0.5 * torch.mean(mean_sq + stddev_sq - torch.log(stddev_sq) - 1)

    def cmp_crit(self, input, target):
        with torch.no_grad():
            self.d(target, save_activation=True)
            target_activation = torch.cat(self.d.activation).detach().clone()
        self.d(input, save_activation=True)
        input_activation = torch.cat(self.d.activation)

        input_activation = torch.sigmoid(input_activation)
        target_activation = torch.sigmoid(target_activation)

        return nn.functional.mse_loss(input_activation, target_activation)

    def forward(
        self,
        x,
        r1_gamma: float = 10.0,
        gamma: float = 1.0,
        beta: float = 1e-4,
        reg_rec: float = 3e0,
        epsilon_drift: float = 1e-3,
    ):
        batch_size = x.shape[0]
        device = x.device

        # loss encoder
        z, mean, std = self.e(x, sample=True)
        x_prime = self.g(z)

        loss_latent = self.latent_loss(mean, std).mean()
        # loss_mse = nn.functional.mse_loss(x_prime, x).mean()  # environ 1.3 au début
        loss_mse = self.cmp_crit(x_prime, x).mean() * reg_rec
        loss_e = beta * loss_latent + loss_mse

        # loss decoder
        z, mean, std = self.e(x, sample=True)
        x_prime = self.g(z)

        z_fake = torch.randn_like(z)
        x_fake = self.g(z_fake)
        d_fake = self.d(x_fake)

        loss_adv = cirtics_activation(-d_fake).mean()
        # loss_mse = nn.functional.mse_loss(x_prime, x).mean()  # environ 1.3 au début
        loss_mse = self.cmp_crit(x_prime, x).mean() * reg_rec
        loss_g = loss_mse + loss_adv / gamma

        # loss discriminator
        d_real = self.d(x)
        z_fake = torch.randn_like(z)
        with torch.no_grad():
            x_fake = self.g(z_fake)
        d_fake = self.d(x_fake)

        real_grads = torch.autograd.grad(
            d_real.mean(), x, create_graph=True, retain_graph=True
        )[0]
        r1_penalty = torch.sum(real_grads.pow(2.0), dim=[1, 2, 3]).mean()
        drifting_penalty = d_real.pow(2.0).mean()
        loss_adv = (
            cirtics_activation(d_fake).mean() + cirtics_activation(-d_real).mean()
        )

        loss_d = (
            epsilon_drift * drifting_penalty + loss_adv + r1_penalty * r1_gamma * 0.5
        )

        # with torch.no_grad():   self.e.requires_grad_(True)
        return loss_e, loss_g, loss_d


def test_hparams(
    epochs=16,
    batch_size=128,
    lr_e=1.0e-3,
    lr_g=1.0e-3,
    lr_d=1.0e-3,
    verbose=True,
):
    from torchvision import datasets, transforms
    import torch.optim as optim
    import torch.autograd as autograd
    from torch.utils.tensorboard import SummaryWriter
    import matplotlib.pyplot as plt
    import numpy as np
    import os
    import uuid

    tmp_dir = "./tmp"

    unique_id = str(uuid.uuid1())

    writer = SummaryWriter(os.path.join(tmp_dir, unique_id))

    device = "cpu"
    if torch.cuda.is_available():
        device = "cuda"

    if verbose:
        print(f"starting on device {device}")

    model = VAEGAN()

    model = model.to(device)

    t = transforms.Compose(
        [
            transforms.Pad(2),
            transforms.ToTensor(),
            transforms.Normalize((0.5,), (0.5,)),
        ]
    )

    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST(
            "./datasets/mnist/data",
            train=True,
            download=True,
            transform=t,
        ),
        batch_size=batch_size,
        shuffle=True,
    )
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST(
            "./datasets/mnist/data",
            train=False,
            transform=t,
        ),
        batch_size=batch_size,
        shuffle=True,
    )

    encoder_optimizer = LREQAdam(
        model.e.parameters(),
        lr=lr_e,
        weight_decay=0,
    )
    generator_optimizer = LREQAdam(
        model.g.parameters(),
        lr=lr_g,
        weight_decay=0,
    )
    discriminator_optimizer = LREQAdam(
        model.d.parameters(),
        lr=lr_d,
        weight_decay=0,
    )
    print(lr_e, lr_g, lr_d)

    losses = []

    for e in range(epochs):
        for i, [x, _] in enumerate(train_loader):
            global_step = i + e * len(train_loader)

            batch_size = x.shape[0]
            x = nn.functional.avg_pool2d(x, kernel_size=2)

            x = x.to(device)
            x.requires_grad = True

            encoder_optimizer.zero_grad()
            generator_optimizer.zero_grad()
            discriminator_optimizer.zero_grad()

            loss_e, loss_g, loss_d = model(x)
            # loss_e, loss_g = model(x)

            loss_e.backward()
            loss_g.backward()
            loss_d.backward()

            encoder_optimizer.step()
            generator_optimizer.step()
            discriminator_optimizer.step()

            with torch.no_grad():
                z = torch.randn((x.shape[0], model.e.z_size), device=x.device)

                x_prime = model.g(z)
                std = torch.std(x_prime.clone().detach(), dim=(0, 1)).mean()

                x_prime = model.g(model.e(x, sample=False))
            diff = nn.functional.mse_loss(x_prime, x).mean()

            if verbose:
                print(
                    f"epoch: {e+1}/{epochs}",
                    f"batch: {i+1}/{len(train_loader)}",
                    f"loss_e: {loss_e.item():6.3f}",
                    f"loss_g: {loss_g.item():6.3f}",
                    f"loss_d: {loss_d.item():6.3f}",
                    f"diff: {diff.item():6.3f}",
                    f"std: {std.item():6.3f}",
                    end="\r",
                )
            losses.append([loss_e.item(), loss_g.item(), loss_d.item()])
            writer.add_scalar("Loss/loss_e/train", loss_e.item(), global_step)
            writer.add_scalar("Loss/loss_g/train", loss_g.item(), global_step)
            writer.add_scalar("Loss/loss_d/train", loss_d.item(), global_step)
            writer.add_scalar("Metrics/diff/train", diff.item(), global_step)
            writer.add_scalar("Metrics/std/train", std.item(), global_step)

        if verbose:
            print()

        n = 8
        inputs = x[:n].detach().cpu()
        outputs = model.g(model.e(x[:n], sample=False)).detach().cpu()

        inputs = torch.cat(list(inputs), dim=2)
        outputs = torch.cat(list(outputs), dim=2)
        tmp = torch.cat([inputs, outputs], dim=1)

        writer.add_image(
            "Identity/train", tmp.numpy() * 0.5 + 0.5, global_step=global_step
        )

        n = 8
        z = torch.randn((n, model.e.z_size), device=x.device)
        outputs = model.g(z).detach().cpu()

        tmp = torch.cat(list(outputs), dim=2)

        writer.add_image(
            "Generated/train", tmp.numpy() * 0.5 + 0.5, global_step=global_step
        )

    total_loss_e = []
    total_loss_d = []
    total_loss_g = []
    total_diff = []
    total_std = []
    model.eval()

    for i, [x, _] in enumerate(test_loader):
        batch_size = x.shape[0]
        x = nn.functional.avg_pool2d(x, kernel_size=2)
        x = x.to(device)
        x.requires_grad = True

        loss_e, loss_g, loss_d = model(x)
        # loss_e, loss_g = model(x)

        with torch.no_grad():
            z = torch.randn((x.shape[0], model.e.z_size), device=x.device)

            x_prime = model.g(z)
            std = torch.std(x_prime.clone().detach(), dim=(0, 1)).mean()

            x_prime = model.g(model.e(x, sample=False))
        diff = nn.functional.mse_loss(x_prime, x).mean()

        if verbose:
            print(f"testing batch: {i+1}/{len(test_loader)}", end="\r")

        total_loss_e.append(loss_e.item())
        total_loss_d.append(loss_d.item())
        total_loss_g.append(loss_g.item())
        total_diff.append(diff.item())
        total_std.append(std.item())

    if verbose:
        print()

    hparams = {
        "epochs": epochs,
        "batch_size": batch_size,
        "lr_g": lr_g,
        "lr_e": lr_e,
        "lr_d": lr_d,
    }
    metrics = {
        "Loss/loss_e/test": torch.mean(torch.tensor(total_loss_e)),
        "Loss/loss_g/test": torch.mean(torch.tensor(total_loss_g)),
        "Loss/loss_d/test": torch.mean(torch.tensor(total_loss_d)),
        "Metrics/diff/test": torch.mean(torch.tensor(total_diff)),
        "Metrics/std/test": torch.mean(torch.tensor(total_std)),
    }

    writer.add_hparams(hparams, metrics)

    writer.close()

    return metrics, unique_id


p0 = torch.tensor([1.0e-4, 1.0e-4, 1.0e-4]).numpy()
epochs = 128
batch_size = 128


def to_eval(p):
    res, unique_id = test_hparams(
        epochs=epochs,
        batch_size=batch_size,
        lr_e=p[0].item(),
        lr_g=p[1].item(),
        lr_d=p[2].item(),
        verbose=False,
    )
    diff = res["Metrics/diff/test"].item()
    std = res["Metrics/std/test"].item()
    loss = diff - 100 * std
    print(f"{p} done! ({diff}, {std})")
    return (unique_id, diff, std, loss, p)


def genetic(input_file: str) -> None:
    import time
    from multiprocessing import Pool
    import json

    import telegram_send
    import numpy as np

    from utils import NumpyEncoder

    t0 = time.time()

    pop_selection = np.array([5, 4, 3, 2, 1, 1])
    ##pop_selection = np.array([2, 1])
    pop_size = pop_selection.sum().item()
    proba_mutation = 0.5
    aleat_mod = 0.1  # multiplier (percentage)

    def mutation(p, mod, proba):  # simple mutation
        res = p.copy()
        random_select = np.random.rand(*p.shape) < proba
        res[random_select] *= (
            1 + (np.random.rand(*p[random_select].shape) / 2 - 1) * mod
        )
        return res

    def hybridation(p):  # simple hybridation
        swap_pop = np.random.randint(low=0, high=p.shape[0], size=(p.shape[0], 2))
        swap_value = np.random.randint(low=0, high=p.shape[1], size=(p.shape[0]))

        res = p.copy()
        for i in range(p.shape[0]):
            res[swap_pop[i, 0], swap_value[i]], res[swap_pop[i, 1], swap_value[i]] = (
                res[swap_pop[i, 1], swap_value[i]],
                res[swap_pop[i, 0], swap_value[i]],
            )
        return res

    if args.input is None:
        iteration = 1
        best = []
        p = np.concatenate([[p0.copy()] for _ in range(pop_size)], axis=0)
    else:
        with open(args.input, "r") as fp:
            data = json.load(fp)
        iteration = data["iteration"] + 1
        best = []
        for e in data["best"]:
            elem = (e[0], e[1], e[2], e[3], np.array(e[4]))
            best.append(elem)
        p = np.concatenate([[e[-1]] for e in best], axis=0)

    while True:

        p = mutation(p, aleat_mod, proba_mutation)
        p = hybridation(p)

        with Pool(3) as pool:
            results = pool.map(to_eval, list(p))

        results = results + best

        results.sort(key=lambda x: x[-2])

        best = results[:pop_size]

        def res2str(res):
            str_var_list = ["{:.4f}".format(v) for v in res[1:-2]]
            return f"({res[0]} {' '.join(str_var_list)})"

        print(
            f"iteration:{iteration}",
            f"best result:{res2str(results[0])}",
            f"current worst:{res2str(results[-1])}",
            f"elapsed time:{time.time()-t0:.1f}s",
        )
        telegram_send.send(
            messages=[
                "\n".join(
                    [
                        f"iteration: {iteration}",
                        f"best result: {res2str(results[0])}",
                        f"current worst: {res2str(results[-1])}",
                        f"elapsed time: {time.time()-t0:.1f}s",
                    ]
                )
            ]
        )

        new_pop = []
        for i, count in enumerate(pop_selection):
            for _ in range(count):
                new_pop.append(best[i][-1])

        p = np.concatenate([[elem.copy()] for elem in new_pop], axis=0)

        with open("state.json", "w") as fp:
            json.dump({"iteration": iteration, "best": best}, fp, cls=NumpyEncoder)

        with open("state.json", "rb") as fp:
            telegram_send.send(files=[fp])

        iteration += 1


def grid() -> None:
    import time
    from multiprocessing import Pool
    import json

    import telegram_send
    import numpy as np

    from utils import NumpyEncoder

    t0 = time.time()

    lr = [5e-4, 3e-4, 2e-4, 5e-5, 3e-5, 2e-5, 5e-6, 3e-6, 2e-6]

    lr1, lr2 = torch.meshgrid(torch.tensor(lr), torch.tensor(lr))
    list_lr = torch.cat([lr1.reshape(-1, 1), lr2.reshape(-1, 1)], dim=1)
    revert = range(list_lr.shape[0])[::-1]
    list_lr = list_lr[revert]

    with Pool(3) as pool:
        results = pool.map(to_eval, list(list_lr))

    results.sort(key=lambda x: x[-2])

    def res2str(res):
        str_var_list = ["{:.4f}".format(v) for v in res[1:-2]]
        return f"({res[0]} {' '.join(str_var_list)})"

    print(
        f"best result:{res2str(results[0])}",
        f"current worst:{res2str(results[-1])}",
        f"elapsed time:{time.time()-t0:.1f}s",
    )
    telegram_send.send(
        messages=[
            str(
                "\n".join(
                    [
                        f"best result: {res2str(results[0])}",
                        f"current worst: {res2str(results[-1])}",
                        f"elapsed time: {time.time()-t0:.1f}s",
                    ]
                )
            )
        ]
    )

    with open("results.json", "w") as fp:
        json.dump(results, fp, cls=NumpyEncoder)

    with open("results.json", "r") as fp:
        telegram_send.send(files=[fp])


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input", "-i", help="restart from a checkpoint in json format", default=None
    )
    parser.add_argument(
        "--genetic",
        "-g",
        help="start hyperparameters optimization with genetic algorithm",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--grid",
        "-G",
        help="start hyperparameters optimization with grid search algorithm",
        action="store_true",
        default=False,
    )

    args = parser.parse_args()

    if args.genetic:
        genetic(args.input)
    elif args.grid:
        grid()
    else:
        test_hparams(
            epochs=epochs,
            batch_size=batch_size,
            lr_e=p0[0].item(),
            lr_g=p0[1].item(),
            lr_d=p0[2].item(),
            verbose=True,
        )
