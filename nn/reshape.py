import torch
import torch.nn as nn


class Reshape(nn.Module):
    def __init__(self, output_size):
        super(Reshape, self).__init__()

        self.output_size = list(output_size)

    def forward(self, x):
        return torch.reshape(x, tuple([-1] + self.output_size))


class DebugShape(nn.Module):
    def __init__(self):
        super(DebugShape, self).__init__()

    def forward(self, x):
        print(x.shape)
        return x
