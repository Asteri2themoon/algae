import torch
import torch.nn as nn


class Noise(nn.Module):
    def __init__(self, std):
        super(Noise, self).__init__()

        self.std = std

    def forward(self, x):
        n = torch.randn_like(x) * self.std
        return x + n
